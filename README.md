# Description

This on-going project is an implementation of [Cycle.js](https://cycle.js.org/) in Angular.
Cycle.js has some cool ideas such as the Human-Machine interaction loop and the Model-View-Intent (MVI) pattern.
However, I am under an impression that Cycle.js works better as architecture ideas or a library than as a full framework.

Currently the project has a basic implementation of Cycle.js in `./src/app/cycle`.
Examples on [Cycle.js website](https://cycle.js.org/basic-examples.html) are implemented using the extension `./src/app/app-cycle`.
This shows a clear potential of Cycle.js being used as a library; thus retain its cool ideas plus all the benefits of an established platform like Angular.

# TODO
-   Flesh out the `Port<I,O>` abstraction
-   An example showing how to make data-flow components

# Run with Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.16.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
