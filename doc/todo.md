# Add an example that uses AppContext input and output

# Let Port have both input and output
Something like:
```txt
interface Port<I, O> {
  send(value: I);
  connect(): Observable<O>;
}
```
