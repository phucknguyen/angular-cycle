import { Injectable } from '@angular/core';

import { Port } from 'src/app/cycle';
import { AppIn, AppOut } from './app-context.io';

import * as rx from 'rxjs';

@Injectable({providedIn: 'root'})
export class AppContext implements Port<AppIn> {
  private _input$ = new rx.Subject<AppIn>();

  readonly output$: rx.Observable<AppOut> = rx.EMPTY;

  readonly initialValues = {
    hello: 'World!',
    toggle: true,
  };

  send(input: AppIn) { this._input$.next(input); }
}
