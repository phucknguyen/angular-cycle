import { Cycle, Sinks } from 'src/app/cycle';
import { AppIn } from './app-context.io';

export abstract class AppCycle<V> extends Cycle<AppIn, V> {}

export class AppSinks<V> extends Sinks<AppIn, V> {}
