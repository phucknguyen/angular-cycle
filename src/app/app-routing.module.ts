import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NavComponent } from './nav/nav.component';
import { HelloComponent } from './hello/hello.component';
import { CounterComponent } from './counter/counter.component';
import { ToggleComponent } from './toggle/toggle.component';
import { CatFactsComponent } from './cat-facts/cat-facts.component';
import { BmiComponent } from './bmi/bmi.component';

const routes: Routes = [
  { path: '', component: NavComponent, pathMatch: 'full' },
  { path: 'hello', component: HelloComponent, pathMatch: 'full' },
  { path: 'counter', component: CounterComponent, pathMatch: 'full' },
  { path: 'toggle', component: ToggleComponent, pathMatch: 'full' },
  { path: 'cat-facts', component: CatFactsComponent, pathMatch: 'full' },
  { path: 'bmi', component: BmiComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
