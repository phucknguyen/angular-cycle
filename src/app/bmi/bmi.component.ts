import { Component, ChangeDetectionStrategy } from '@angular/core';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import { AppCycle, AppSinks } from 'src/app/app-cycle';
import { DomIn, View } from './bmi.io';
import { process } from './bmi.process';

@Component({
  selector: 'app-bmi',
  templateUrl: './bmi.component.html',
  styleUrls: ['./bmi.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BmiComponent extends AppCycle<View> {
  weight$ = new rx.Subject<string>();
  weightInput(value: string) { this.weight$.next(value); }

  height$ = new rx.Subject<string>();
  heightInput(value: string) { this.height$.next(value); }

  process(): AppSinks<View> {
    const initialState: DomIn = {weight: '70', height: '170'};
    const intent$: rx.Observable<DomIn> = rx.merge(
      this.weight$.pipe(op.map(weight => ({weight}))),
      this.height$.pipe(op.map(height => ({height}))),
    ).pipe(
      op.startWith(initialState)
    );
    return process(intent$);
  }
}

