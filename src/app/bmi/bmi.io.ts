export class DomIn {
  weight?: string; height?: string;
}

export class View {
  weight!: number;
  height!: number;
  get bmi(): number {
    const heightMeters = this.height * 0.01;
    const bmi = Math.round(this.weight / (heightMeters * heightMeters));
    return bmi;
  }

  constructor(data: {weight: number; height: number;}) {
    this.weight = data.weight; this.height = data.height;
  }
}

