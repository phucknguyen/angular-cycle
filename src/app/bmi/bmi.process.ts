import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import { AppSinks } from 'src/app/app-cycle';
import { DomIn, View } from './bmi.io';

function model(intent$: rx.Observable<DomIn>): rx.Observable<DomIn> {
  return intent$.pipe(
    op.scan<DomIn, DomIn>(
      (acc, val) => ({...acc, ...val})
    )
  );
}

function view(model$: rx.Observable<DomIn>): rx.Observable<View> {
  return model$.pipe(
    op.filter<DomIn>(({weight, height}) => {
      if (!weight || !height) { return false; }
      if (!Number(weight) || !Number(height)) { return false; }
      return true;
    }),
    op.map<DomIn, View>(({weight, height}) => {
      return new View({
        weight: Number(weight), height: Number(height)
      });
    }),
  );
}

export function process(intent$: rx.Observable<DomIn>): AppSinks<View> {
  const view$: rx.Observable<View> = view(model(intent$));
  return {DOM: view$};
}
