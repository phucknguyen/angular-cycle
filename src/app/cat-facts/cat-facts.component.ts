import { Component, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import { HttpCmd, HttpDriver, HttpResult } from 'src/app/cycle';
import { AppCycle, AppSinks } from 'src/app/app-cycle';

@Component({
  selector: 'app-cat-facts',
  templateUrl: './cat-facts.component.html',
  styleUrls: ['./cat-facts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CatFactsComponent extends AppCycle<View> {
  private getCatFacts$ = new rx.Subject<any>();
  getCatFacts(): void { this.getCatFacts$.next(true); }

  constructor(private httpDriver: HttpDriver) {
    super();
  }

  process(): AppSinks<View> {
    const getCatFacts$: rx.Observable<HttpCmd> =
    this.getCatFacts$.pipe(
      op.map<any, HttpCmd>(trigger => getCatFacts())
    );

    const view$: rx.Observable<View> =
    this.httpDriver.select('facts').pipe(
      op.map<HttpResult, View>(toView),
      op.filter(facts => !!facts),
      op.startWith(new View()),
    );

    return {
      DOM: view$,
      HTTP: {
        output: getCatFacts$,
        port: this.httpDriver,
      }
    };
  }
}

class CatFact {
  id!: string;
  text!: string;
  type!: string;
}

class View {
  facts?: CatFact[] = [];
  error?: any;
}

function getCatFacts(): HttpCmd {
  return {
    url: '/api/cat-facts/facts/random?animal_type=cat&amount=3',
    method: 'GET',
    category: 'facts',
  };
}

function toView(result: HttpResult): View {
  if (result.body) {
    const facts: CatFact[] = toCatFacts(result.body as any[]);
    return {facts};
  }
  if (result.error) {
    return {error: result.error};
  }
  return null;
}

function toCatFacts(facts: any[]): CatFact[] {
  return facts.map(fact => ({
    id: fact._id,
    text: fact.text,
    type: fact.type,
  }));
}
