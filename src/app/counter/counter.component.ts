import { Component, ChangeDetectionStrategy } from '@angular/core';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import { AppCycle, AppSinks } from 'src/app/app-cycle';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterComponent extends AppCycle<View> {
  private decrement$ = new rx.Subject<any>();
  decrease(): void { this.decrement$.next(true); }

  private increment$ = new rx.Subject<any>();
  increase(): void { this.increment$.next(true); }

  process(): AppSinks<View> {
    const initial = 1;

    const diff$: rx.Observable<number> = rx.merge(
      this.decrement$.pipe(op.mapTo(-1)),
      this.increment$.pipe(op.mapTo(1)),
    );

    const view$: rx.Observable<View> = diff$.pipe(
      op.scan((x,y) => x+y, initial),
      op.startWith(initial),
      op.map(count => ({count})),
    );

    return {DOM: view$};
  }
}

class View {
  count!: number;
}
