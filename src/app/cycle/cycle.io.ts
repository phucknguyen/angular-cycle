import * as rx from 'rxjs';

import { Port } from './port';
import { HttpCmd } from './http.io';

export class Sinks<O, V> {
  readonly DOM: rx.Observable<V> = rx.EMPTY;
  readonly HTTP?: {
    output: rx.Observable<HttpCmd>;
    port: Port<HttpCmd>;
  };
  readonly CONTEXT?: {
    output: rx.Observable<O>;
    port: Port<O>
  }
}
