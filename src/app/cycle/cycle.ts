import { OnInit, OnDestroy, Injectable } from '@angular/core';

import { Sinks } from './cycle.io';
import { Port } from './port';
import { HttpCmd, HttpResult } from './http.io';
import { HttpDriver } from './http-driver';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

export abstract class Cycle<O, V> implements OnInit, OnDestroy {
  private subscription = new rx.Subscription();

  private _view$: rx.Observable<V>;
  get view$(): rx.Observable<V> { return this._view$ || rx.EMPTY; }

  ngOnInit(): void {
    const sinks: Sinks<O, V> = this.process();
    if (sinks.DOM) {
      this._view$ = sinks.DOM;
    }
    if (sinks.HTTP) {
      const output: rx.Observable<HttpCmd> = sinks.HTTP.output;
      const port: Port<HttpCmd> = sinks.HTTP.port;
      this.subscription.add(
        output.subscribe(value => port.send(value))
      );
    }
    if (sinks.CONTEXT) {
      const output: rx.Observable<O> = sinks.CONTEXT.output;
      const port: Port<O> = sinks.CONTEXT.port;
      this.subscription.add(
        output.subscribe(value => port.send(value))
      );
    }
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  abstract process(): Sinks<O, V>;
}
