import { Injectable } from '@angular/core';
import {
  HttpClient, HttpRequest, HttpEvent, HttpResponse
} from '@angular/common/http';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import { Port } from './port';
import { HttpCmd, HttpResult } from './http.io';

@Injectable({providedIn: 'root'})
export class HttpDriver implements Port<HttpCmd> {
  private cmd$ = new rx.Subject<HttpCmd>();

  private responses$: rx.Observable<{
    category: string; response$: rx.Observable<HttpResult>
  }> = this.cmd$.pipe(
    op.map(({category, method, url}) => {
      const request = new HttpRequest<any>(method, url);
      const response$: rx.Observable<HttpResult> =
        this.http.request(request).pipe(
          op.map<HttpEvent<any>, HttpResult>(this.toHttpResult),
          op.catchError(error => rx.of({error}))
        );
      return {category, response$};
    }),
  );

  constructor(private http: HttpClient) { }

  private toHttpResult(event: HttpEvent<any>): HttpResult {
    if (event instanceof HttpResponse) {
      return {body: event.body};
    }
    else {
      return new HttpResult();
    }
  }

  send(cmd: HttpCmd): void {
    this.cmd$.next(cmd);
  }

  select(category: string): rx.Observable<HttpResult> {
    return this.responses$.pipe(
      op.filter(res => (res.category === category)),
      op.mergeMap(res => res.response$),
    );
  }
}

