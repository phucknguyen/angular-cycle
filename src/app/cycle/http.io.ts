export class HttpCmd {
  category!: string;
  method!: "DELETE" | "GET" | "HEAD" | "JSONP" | "OPTIONS";
  url!: string;
}

export class HttpResult {
  body?: any;
  error?: any;
}

