export interface Port<O> {
  send(value: O): void;
}
