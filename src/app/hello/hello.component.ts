import { Component, ChangeDetectionStrategy } from '@angular/core';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import {
  AppCycle, AppSinks, AppContext
} from 'src/app/app-cycle';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HelloComponent extends AppCycle<string> {
  private input$ = new rx.Subject<string>();
  input(value: string): void {this.input$.next(value);}

  constructor(private context: AppContext) {
    super();
  }

  process(): AppSinks<string> {
    const initialName = this.context.initialValues.hello;
    const name$: rx.Observable<string> = this.input$.pipe(
      op.startWith(initialName)
    );
    return {DOM: name$};
  }
}
