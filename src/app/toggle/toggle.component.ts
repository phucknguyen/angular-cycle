import { Component, ChangeDetectionStrategy } from '@angular/core';

import * as rx from 'rxjs';
import * as op from 'rxjs/operators';

import {
  AppCycle, AppSinks, AppContext
} from 'src/app/app-cycle';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToggleComponent extends AppCycle<View> {
  private checkboxClick$ = new rx.Subject<any>();
  click(): void {this.checkboxClick$.next(true);}

  constructor(private context: AppContext) {
    super();
  }

  process(): AppSinks<View> {
    const initialChecked = this.context.initialValues.toggle;

    const view$: rx.Observable<View> = this.checkboxClick$.pipe(
      op.scan((checked, toggle) => !checked, initialChecked),
      op.map(checked => new View(checked)),
      op.startWith(new View(initialChecked)),
    );

    return {DOM: view$};
  }
}

class View {
  get status(): string {
    return (this.checked ? 'On' : 'Off');
  }
  constructor(readonly checked: boolean) {}
}
